package com.meusalao.domain.dtos;

import com.meusalao.domain.entities.Address;

public class AddressDto {

	private Long id;

	private String address;

	private String zipCode;

	private String neighborhood;

	private String city;

	private String state;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress( String address ) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode( String zipCode ) {
		this.zipCode = zipCode;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood( String neighborhood ) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity( String city ) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState( String state ) {
		this.state = state;
	}

	public Address toEntity() {
		Address address = new Address();
		address.setId( this.id );
		address.setAddress( this.address );
		address.setNeighborhood( this.neighborhood );
		address.setCity( this.city );
		address.setState( this.state );
		address.setZipCode( this.zipCode );
		return address;
	}

}
