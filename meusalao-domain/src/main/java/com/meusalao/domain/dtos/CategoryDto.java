package com.meusalao.domain.dtos;

import com.meusalao.domain.entities.Category;

public class CategoryDto {

	private Long id;

	private String description;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public Category toEntity() {
		Category category = new Category();
		category.setId( this.id );
		category.setDescription( this.description );
		return category;
	}
}
