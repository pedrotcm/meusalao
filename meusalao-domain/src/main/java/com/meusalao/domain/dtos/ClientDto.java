package com.meusalao.domain.dtos;

import com.meusalao.domain.entities.Client;
import com.meusalao.domain.enums.ChannelsMetSalon;

public class ClientDto extends PersonDto {

	private ChannelsMetSalon channelMetSalon;

	private String observations;
	

	public ChannelsMetSalon getChannelMetSalon() {
		return channelMetSalon;
	}

	public void setChannelMetSalon( ChannelsMetSalon channelMetSalon ) {
		this.channelMetSalon = channelMetSalon;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations( String observations ) {
		this.observations = observations;
	}

	public Client toEntity() {
		Client client = new Client();
		client.setId( this.id );
		client.setName( this.name );
		client.setBirthday( this.birthday );
		client.setPhone( this.phone );
		client.setPhone2( this.phone2 );
		client.setEmail( this.email );
		if ( this.address != null ) {
			client.setAddress( this.address.toEntity() );
		}
		client.setChannelMetSalon( this.channelMetSalon );
		client.setObservations( this.observations );
		return client;
	}
}
