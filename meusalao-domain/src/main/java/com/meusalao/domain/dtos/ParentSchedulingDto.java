package com.meusalao.domain.dtos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.enums.Frequency;

public class ParentSchedulingDto {
	
	private Long id;

	private Long idScheduling;

	private Date dateScheduled;

	private ClientDto client;

	private Frequency frequency;

	private List<SchedulingDto> scheduledServices;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public Long getIdScheduling() {
		return idScheduling;
	}

	public void setIdScheduling( Long idScheduling ) {
		this.idScheduling = idScheduling;
	}

	public ClientDto getClient() {
		return client;
	}

	public void setClient( ClientDto client ) {
		this.client = client;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency( Frequency frequency ) {
		this.frequency = frequency;
	}

	public List<SchedulingDto> getScheduledServices() {
		return scheduledServices;
	}

	public void setScheduledServices( List<SchedulingDto> scheduledServices ) {
		this.scheduledServices = scheduledServices;
	}

	public Date getDateScheduled() {
		return dateScheduled;
	}

	public void setDateScheduled( Date dateScheduled ) {
		this.dateScheduled = dateScheduled;
	}

	public ParentScheduling toEntity() {
		ParentScheduling parentScheduling = new ParentScheduling();
		parentScheduling.setCreationDate( new Date() );
		parentScheduling.setId( this.id );
		parentScheduling.setFrequency( this.frequency );
		parentScheduling.setSchedulings( this.getSchedulings( parentScheduling ) );
		return parentScheduling;
	}

	private List<Scheduling> getSchedulings( ParentScheduling scheduling ) {
		List<Scheduling> scheduledServices = new ArrayList<>();
		this.scheduledServices.forEach( scheduledService -> {
			scheduledServices.add( scheduledService.toEntity( this.idScheduling, scheduling, this.dateScheduled, this.client ) );
		} );
		return scheduledServices;
	}

}
