package com.meusalao.domain.dtos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.enums.Period;
import com.meusalao.domain.enums.Status;

public class ParentUnavailableDto {
	
	private Long id;

	private Long idScheduling;

	private ProfessionalDto professional;

	private Date dateUnavailable;

	private Period period;

	private Integer startTimeUnavailable;

	private Integer endTimeUnavailable;

	private String reasonUnavailable;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public Long getIdScheduling() {
		return idScheduling;
	}

	public void setIdScheduling( Long idScheduling ) {
		this.idScheduling = idScheduling;
	}

	public ProfessionalDto getProfessional() {
		return professional;
	}

	public void setProfessional( ProfessionalDto professional ) {
		this.professional = professional;
	}

	public Date getDateUnavailable() {
		return dateUnavailable;
	}

	public void setDateUnavailable( Date dateUnavailable ) {
		this.dateUnavailable = dateUnavailable;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod( Period period ) {
		this.period = period;
	}

	public Integer getStartTimeUnavailable() {
		return startTimeUnavailable;
	}

	public void setStartTimeUnavailable( Integer startTimeUnavailable ) {
		this.startTimeUnavailable = startTimeUnavailable;
	}

	public Integer getEndTimeUnavailable() {
		return endTimeUnavailable;
	}

	public void setEndTimeUnavailable( Integer endTimeUnavailable ) {
		this.endTimeUnavailable = endTimeUnavailable;
	}

	public String getReasonUnavailable() {
		return reasonUnavailable;
	}

	public void setReasonUnavailable( String reasonUnavailable ) {
		this.reasonUnavailable = reasonUnavailable;
	}

	public ParentScheduling toEntity() {

		ParentScheduling parentUnavailable = new ParentScheduling();
		parentUnavailable.setCreationDate( new Date() );
		parentUnavailable.setId( this.id );
		parentUnavailable.setPeriod( this.period );
		parentUnavailable.setSchedulings( this.getUnavailables( parentUnavailable ) );
		return parentUnavailable;
	}

	private List<Scheduling> getUnavailables( ParentScheduling parentUnavailable ) {
		List<Scheduling> unavailables = new ArrayList<>();
		Scheduling unavailable = new Scheduling();
		unavailable.setId( this.idScheduling );
		unavailable.setIsUnavailable( true );
		unavailable.setProfessional( this.professional.toEntity() );
		unavailable.setParentScheduling( parentUnavailable );
		Calendar c = Calendar.getInstance();
		c.setTime( this.dateUnavailable );
		c.set( Calendar.HOUR_OF_DAY, 0 );
		c.set( Calendar.SECOND, 0 );
		c.set( Calendar.MILLISECOND, 0 );
		c.set( Calendar.MINUTE, this.startTimeUnavailable );
		unavailable.setDateScheduled( c.getTime() );
		c = Calendar.getInstance();
		c.setTime( this.dateUnavailable );
		c.set( Calendar.HOUR_OF_DAY, 0 );
		c.set( Calendar.SECOND, 0 );
		c.set( Calendar.MILLISECOND, 0 );
		c.add( Calendar.MINUTE, this.endTimeUnavailable );
		unavailable.setDateEndPrevision( c.getTime() );
		unavailable.setReasonUnavailable( this.reasonUnavailable );
		unavailable.setStartTimeUnavailable( this.startTimeUnavailable );
		unavailable.setEndTimeUnavailable( this.endTimeUnavailable );
		unavailable.setStatus( Status.UNAVAILABLE );
		unavailables.add( unavailable );
		return unavailables;
	}

}
