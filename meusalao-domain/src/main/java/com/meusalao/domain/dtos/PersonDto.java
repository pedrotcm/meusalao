package com.meusalao.domain.dtos;

public class PersonDto {

	protected Long id;

	protected String name;

	protected String birthday;

	protected String phone;

	protected String phone2;

	protected String email;

	protected AddressDto address;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday( String birthday ) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone( String phone ) {
		this.phone = phone;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2( String phone2 ) {
		this.phone2 = phone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setAddress( AddressDto address ) {
		this.address = address;
	}

}
