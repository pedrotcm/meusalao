package com.meusalao.domain.dtos;

import com.meusalao.domain.entities.Price;
import com.meusalao.domain.entities.Service;

public class PriceDto {

	private Long id;

	private String complementOfName;

	private Double value;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getComplementOfName() {
		return complementOfName;
	}

	public void setComplementOfName( String complementOfName ) {
		this.complementOfName = complementOfName;
	}

	public Double getValue() {
		return value;
	}

	public void setValue( Double value ) {
		this.value = value;
	}

	public Price toEntity( Service service ) {
		Price price = new Price();
		price.setId( this.id );
		price.setComplementOfName( this.complementOfName );
		price.setValue( this.value );
		price.setService( service );
		return price;
	}

}
