package com.meusalao.domain.dtos;

import java.util.ArrayList;
import java.util.List;

import com.meusalao.domain.entities.Professional;
import com.meusalao.domain.entities.Service;
import com.meusalao.domain.enums.ContractType;

public class ProfessionalDto extends PersonDto {


	private String nickname;

	private ContractType contractType;

	private Boolean justArrivalOrder;

	private String rg;

	private String cpf;

	private List<ServiceListDto> services;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId( Long id ) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName( String name ) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname( String nickname ) {
		this.nickname = nickname;
	}

	public ContractType getContractType() {
		return contractType;
	}

	public void setContractType( ContractType contractType ) {
		this.contractType = contractType;
	}

	public Boolean getJustArrivalOrder() {
		return justArrivalOrder;
	}

	public void setJustArrivalOrder( Boolean justArrivalOrder ) {
		this.justArrivalOrder = justArrivalOrder;
	}

	@Override
	public String getBirthday() {
		return birthday;
	}

	@Override
	public void setBirthday( String birthday ) {
		this.birthday = birthday;
	}

	@Override
	public String getPhone() {
		return phone;
	}

	@Override
	public void setPhone( String phone ) {
		this.phone = phone;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail( String email ) {
		this.email = email;
	}

	@Override
	public AddressDto getAddress() {
		return address;
	}

	@Override
	public void setAddress( AddressDto address ) {
		this.address = address;
	}

	public String getRg() {
		return rg;
	}

	public void setRg( String rg ) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf( String cpf ) {
		this.cpf = cpf;
	}

	// public List<ServiceDto> getServices() {
	// return services;
	// }
	//
	// public void setServices( List<ServiceDto> services ) {
	// this.services = services;
	// }

	public Professional toEntity() {
		Professional professional = new Professional();
		professional.setId( this.id );
		professional.setName( this.name );
		professional.setNickname( this.nickname );
		professional.setContractType( this.contractType );
		professional.setJustArrivalOrder( this.justArrivalOrder );
		professional.setBirthday( this.birthday );
		professional.setPhone( this.phone );
		professional.setEmail( this.email );
		professional.setAddress( this.address.toEntity() );
		professional.setRg( this.rg );
		professional.setCpf( this.cpf );
		if ( this.services != null ) {
			professional.setServices( this.getServicesEntities() );
		}
		return professional;
	}

	public List<ServiceListDto> getServices() {
		return services;
	}

	public void setServices( List<ServiceListDto> services ) {
		this.services = services;
	}

	private List<Service> getServicesEntities() {
		List<Service> services = new ArrayList<>();
		for ( ServiceListDto serviceList : this.services ) {
			if ( serviceList.getServiceGroup() != null ) {
				serviceList.getServiceGroup().forEach( service -> {
					Service serviceEntity = new Service();
					serviceEntity.setId( service.getId() );
					services.add( serviceEntity );
				} );
			}
		}
		return services;
	}

}
