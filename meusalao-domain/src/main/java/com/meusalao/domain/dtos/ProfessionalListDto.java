package com.meusalao.domain.dtos;

public class ProfessionalListDto {

	private ProfessionalDto professional;

	public ProfessionalDto getProfessional() {
		return professional;
	}

	public void setProfessional( ProfessionalDto professional ) {
		this.professional = professional;
	}

}
