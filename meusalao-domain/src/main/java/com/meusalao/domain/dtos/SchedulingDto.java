package com.meusalao.domain.dtos;

import java.util.Calendar;
import java.util.Date;

import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.enums.Status;

public class SchedulingDto {

	private Long id;

	private ServiceDto service;

	private ProfessionalDto professional;

	private Integer startTime;

	private ParentSchedulingDto parentScheduling;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public ServiceDto getService() {
		return service;
	}

	public void setService( ServiceDto service ) {
		this.service = service;
	}

	public ProfessionalDto getProfessional() {
		return professional;
	}

	public void setProfessional( ProfessionalDto professional ) {
		this.professional = professional;
	}

	public Integer getStartTime() {
		return startTime;
	}

	public void setStartTime( Integer startTime ) {
		this.startTime = startTime;
	}

	public ParentSchedulingDto getParentScheduling() {
		return parentScheduling;
	}

	public void setParentScheduling( ParentSchedulingDto parentScheduling ) {
		this.parentScheduling = parentScheduling;
	}

	public Scheduling toEntity(Long idScheduling, ParentScheduling parentScheduling, Date dateScheduled, ClientDto client ) {
		Scheduling scheduling = new Scheduling();
		scheduling.setId( idScheduling );
		scheduling.setClient( client.toEntity() );
		scheduling.setService( this.service.toEntity() );
		scheduling.setProfessional( this.professional.toEntity() );
		Calendar c = Calendar.getInstance();
		c.setTime( dateScheduled );
		c.set( Calendar.HOUR_OF_DAY, 0 );
		c.set( Calendar.SECOND, 0 );
		c.set( Calendar.MILLISECOND, 0 );
		c.set( Calendar.MINUTE, this.startTime );
		scheduling.setDateScheduled( c.getTime() );
		int endTime = (int) ( Math.ceil( this.service.getTimePrediction() / 10.0 ) * 10 );
		c.add( Calendar.MINUTE, endTime );
		scheduling.setDateEndPrevision( c.getTime() );
		scheduling.setStartTimeValue( this.startTime );
		scheduling.setStatus( Status.SCHEDULED );
		scheduling.setParentScheduling( parentScheduling );
		return scheduling;
	}

	// private List<Professional> getProfessionalsEntities() {
	// List<Professional> professionals = new ArrayList<>();
	// for ( ProfessionalListDto professionalList : this.professionals ) {
	// Professional entity = new Professional();
	// entity.setId( professionalList.getProfessional().getId() );
	// professionals.add( entity );
	// }
	// return professionals;
	// }

}
