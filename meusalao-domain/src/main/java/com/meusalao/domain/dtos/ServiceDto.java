package com.meusalao.domain.dtos;

import java.util.ArrayList;
import java.util.List;

import com.meusalao.domain.entities.Price;
import com.meusalao.domain.entities.Professional;
import com.meusalao.domain.entities.Service;
import com.meusalao.domain.enums.TypeValue;

public class ServiceDto {

	private Long id;

	private String name;

	private Integer commission;

	private Integer timePrediction;

	private Boolean visibleToClient;

	private TypeValue typeValue;

	private List<PriceDto> prices;

	private Long idValueUnique;

	private Double valueUnique;

	private CategoryDto category;

	private List<ProfessionalDto> professionals;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission( Integer commission ) {
		this.commission = commission;
	}

	public Integer getTimePrediction() {
		return timePrediction;
	}

	public void setTimePrediction( Integer timePrediction ) {
		this.timePrediction = timePrediction;
	}

	public Boolean getVisibleToClient() {
		return visibleToClient;
	}

	public void setVisibleToClient( Boolean visibleToClient ) {
		this.visibleToClient = visibleToClient;
	}

	public TypeValue getTypeValue() {
		return typeValue;
	}

	public void setTypeValue( TypeValue typeValue ) {
		this.typeValue = typeValue;
	}

	public List<PriceDto> getPrices() {
		return prices;
	}

	public void setPrices( List<PriceDto> prices ) {
		this.prices = prices;
	}

	public Long getIdValueUnique() {
		return idValueUnique;
	}

	public void setIdValueUnique( Long idValueUnique ) {
		this.idValueUnique = idValueUnique;
	}

	public Double getValueUnique() {
		return valueUnique;
	}

	public void setValueUnique( Double valueUnique ) {
		this.valueUnique = valueUnique;
	}

	public CategoryDto getCategory() {
		return category;
	}

	public void setCategory( CategoryDto category ) {
		this.category = category;
	}

	public List<ProfessionalDto> getProfessionals() {
		return professionals;
	}

	public void setProfessionals( List<ProfessionalDto> professionals ) {
		this.professionals = professionals;
	}

	public com.meusalao.domain.entities.Service toEntity() {
		com.meusalao.domain.entities.Service service = new com.meusalao.domain.entities.Service();
		service.setId( this.id );
		service.setName( this.name );
		if ( this.category != null ) {
			service.setCategory( this.category.toEntity() );
		}
		service.setVisibleToClient( this.visibleToClient );
		service.setCommission( this.commission );
		service.setTimePrediction( this.timePrediction );
		service.setTypeValue( this.typeValue );
		service.setPrices( this.getPrices( service ) );
		if ( this.professionals != null ) {
			service.setProfessionals( this.getProfessionalsEntities() );
		}
		return service;
	}

	private List<Price> getPrices( Service service ) {
		List<Price> prices = new ArrayList<>();
		if ( this.prices != null && !this.prices.isEmpty() ) {
			for ( PriceDto price : this.prices ) {
				prices.add( price.toEntity( service ) );
			}
		} else {
			Price price = new Price();
			price.setId( this.idValueUnique );
			price.setValue( this.valueUnique );
			price.setService( service );
			prices.add( price );
		}
		return prices;
	}

	private List<Professional> getProfessionalsEntities() {
		List<Professional> professionals = new ArrayList<>();
		for ( ProfessionalDto professional : this.professionals ) {
			Professional entity = new Professional();
			entity.setId( professional.getId() );
			professionals.add( entity );
		}
		return professionals;
	}

}
