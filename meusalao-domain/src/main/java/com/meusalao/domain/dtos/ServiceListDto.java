package com.meusalao.domain.dtos;

import java.util.List;

public class ServiceListDto {

	List<ServiceDto> serviceGroup;

	public List<ServiceDto> getServiceGroup() {
		return serviceGroup;
	}

	public void setServiceGroup( List<ServiceDto> serviceGroup ) {
		this.serviceGroup = serviceGroup;
	}

}
