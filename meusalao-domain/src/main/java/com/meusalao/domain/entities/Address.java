package com.meusalao.domain.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table( name = "ADDRESS" )
// @JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class,
// property = "id", scope = Address.class )
@JsonIgnoreProperties( { "hibernateLazyInitializer", "handler" } )
public class Address implements Serializable {

	private static final long serialVersionUID = -143755714336570475L;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	@Column( name = "ADDRESS" )
	private String address;

	@Column( name = "ZIP_CODE" )
	private String zipCode;

	@Column( name = "NEIGHBORHOOD" )
	private String neighborhood;

	@Column( name = "CITY" )
	private String city;

	@Column( name = "STATE" )
	private String state;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress( String address ) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode( String zipCode ) {
		this.zipCode = zipCode;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood( String neighborhood ) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity( String city ) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState( String state ) {
		this.state = state;
	}

}