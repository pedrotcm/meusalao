package com.meusalao.domain.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.meusalao.domain.enums.AuthorityCode;

@Entity
@Table(name = "AUTHORITY")
public class Authority implements Serializable{

	private static final long serialVersionUID = 903087477051306480L;

	@Id
    @Column(name = "ID")
	// @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
	// "authority_seq")
	// @SequenceGenerator(name = "authority_seq", sequenceName =
	// "authority_seq", allocationSize = 1)
	@GeneratedValue( strategy = GenerationType.AUTO )
    private Long id;

	@Column( name = "DESCRIPTION", length = 50 )
	@NotNull
	private String description;

	@Column( name = "CODE", length = 50 )
    @NotNull
    @Enumerated(EnumType.STRING)
	private AuthorityCode code;

    @ManyToMany(mappedBy = "authorities", fetch = FetchType.LAZY)
	private List<Role> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public AuthorityCode getCode() {
		return code;
	}

	public void setCode( AuthorityCode code ) {
		this.code = code;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles( List<Role> roles ) {
		this.roles = roles;
	}

}