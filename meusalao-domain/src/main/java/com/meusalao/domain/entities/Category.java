package com.meusalao.domain.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table( name = "CATEGORY" )
@JsonIgnoreProperties( { "hibernateLazyInitializer", "handler" } )
public class Category implements Serializable{

	private static final long serialVersionUID = -3326438850650210920L;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	@Column( name = "DESCRIPTION" )
	private String description;

	@OneToMany( fetch = FetchType.LAZY, mappedBy = "category" )
	private List<Service> services;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}
	//
	// public List<Service> getServices() {
	// return services;
	// }
	//
	// public void setServices( List<Service> services ) {
	// this.services = services;
	// }

}
