package com.meusalao.domain.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.meusalao.domain.enums.ChannelsMetSalon;

@Entity
@Table( name = "CLIENT" )
public class Client extends Person{

	private static final long serialVersionUID = 2575608023190272504L;

	@Column( name = "CHANNEL_MET_SALON" )
	@Enumerated( EnumType.STRING )
	private ChannelsMetSalon channelMetSalon;

	@Column( name = "OBSERVATIONS" )
	private String observations;

	@OneToMany( mappedBy = "client", cascade = CascadeType.REMOVE )
	@JsonBackReference
	private List<Scheduling> schedulings;

	public ChannelsMetSalon getChannelMetSalon() {
		return channelMetSalon;
	}

	public void setChannelMetSalon( ChannelsMetSalon channelMetSalon ) {
		this.channelMetSalon = channelMetSalon;
	}

	public String getObservations() {
		return observations;
	}

	public void setObservations( String observations ) {
		this.observations = observations;
	}

	public List<Scheduling> getSchedulings() {
		return schedulings;
	}

	public void setSchedulings( List<Scheduling> schedulings ) {
		this.schedulings = schedulings;
	}

}
