package com.meusalao.domain.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.meusalao.domain.enums.Frequency;
import com.meusalao.domain.enums.Period;

@Entity
@Table( name = "PARENT_SCHEDULING" )
public class ParentScheduling implements Serializable {

	private static final long serialVersionUID = 9132635752626833927L;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	@Column( name = "CREATION_DATE" )
	@Temporal( TemporalType.TIMESTAMP )
	private Date creationDate;

	@Column( name = "FREQUENCY" )
	@Enumerated( EnumType.STRING )
	private Frequency frequency;

	@Column( name = "PERIOD" )
	@Enumerated( EnumType.STRING )
	private Period period;

	@OneToMany( mappedBy = "parentScheduling", cascade = CascadeType.ALL, orphanRemoval = true )
	@JsonManagedReference
	private List<Scheduling> schedulings;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate( Date creationDate ) {
		this.creationDate = creationDate;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency( Frequency frequency ) {
		this.frequency = frequency;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod( Period period ) {
		this.period = period;
	}

	public List<Scheduling> getSchedulings() {
		return schedulings;
	}

	public void setSchedulings( List<Scheduling> schedulings ) {
		this.schedulings = schedulings;
	}


}
