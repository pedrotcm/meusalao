package com.meusalao.domain.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@MappedSuperclass
public class Person implements Serializable {

	private static final long serialVersionUID = -5671860807310925600L;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	@Column( name = "NAME" )
	private String name;

	@Column( name = "BIRTHDAY" )
	private String birthday;

	@Column( name = "PHONE" )
	private String phone;

	@Column( name = "PHONE_2" )
	private String phone2;

	@Column( name = "EMAIL" )
	private String email;

	@OneToOne( fetch = FetchType.LAZY, cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_ADDRESS" )
	private Address address;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday( String birthday ) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone( String phone ) {
		this.phone = phone;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2( String phone2 ) {
		this.phone2 = phone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress( Address address ) {
		this.address = address;
	}

}
