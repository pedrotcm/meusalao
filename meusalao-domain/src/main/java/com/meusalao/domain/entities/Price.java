package com.meusalao.domain.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "PRICE")
// @JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class,
// property = "id", scope = Price.class )
public class Price implements Serializable{

	private static final long serialVersionUID = -1612289674483184823L;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	
	@Column( name = "COMPLEMENT_OF_NAME" )
	private String complementOfName;

	@Column( name = "VALUE" )
	private Double value;

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "FK_ID_SERVICE" )
	@JsonIgnore
	private Service service;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getComplementOfName() {
		return complementOfName;
	}

	public void setComplementOfName( String complementOfName ) {
		this.complementOfName = complementOfName;
	}

	public Double getValue() {
		return value;
	}

	public void setValue( Double value ) {
		this.value = value;
	}

	public Service getService() {
		return service;
	}

	public void setService( Service service ) {
		this.service = service;
	}

}

