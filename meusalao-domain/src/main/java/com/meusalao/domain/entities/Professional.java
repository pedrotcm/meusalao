package com.meusalao.domain.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.meusalao.domain.enums.ContractType;

@Entity
@Table( name = "PROFESSIONAL" )
@JsonIgnoreProperties( { "hibernateLazyInitializer", "handler" } )
public class Professional extends Person {

	private static final long serialVersionUID = -1928640808595521535L;

	@Column( name = "NICKNAME" )
	private String nickname;

	@Column( name = "CONTRACT_TYPE" )
	@Enumerated( EnumType.STRING )
	private ContractType contractType;

	@Column( name = "JUST_ARRIVAL_ORDER" )
	private Boolean justArrivalOrder;

	@Column( name = "RG" )
	private String rg;

	@Column( name = "CPF" )
	private String cpf;

	@ManyToMany
	@JoinTable( name = "professional_service", joinColumns = { @JoinColumn( name = "fk_id_professional" ) }, inverseJoinColumns = { @JoinColumn( name = "fk_id_service" ) } )
	@JsonBackReference
	private List<Service> services;

	public String getNickname() {
		return nickname;
	}

	public void setNickname( String nickname ) {
		this.nickname = nickname;
	}

	public ContractType getContractType() {
		return contractType;
	}

	public void setContractType( ContractType contractType ) {
		this.contractType = contractType;
	}

	public Boolean getJustArrivalOrder() {
		return justArrivalOrder;
	}

	public void setJustArrivalOrder( Boolean justArrivalOrder ) {
		this.justArrivalOrder = justArrivalOrder;
	}


	public String getRg() {
		return rg;
	}

	public void setRg( String rg ) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf( String cpf ) {
		this.cpf = cpf;
	}

	public List<Service> getServices() {
		if ( services == null ) {
			services = new ArrayList<>();
		}
		return services;
	}

	public void setServices( List<Service> services ) {
		this.services = services;
	}

}
