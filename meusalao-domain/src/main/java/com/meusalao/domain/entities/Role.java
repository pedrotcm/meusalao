package com.meusalao.domain.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table( name = "ROLE" )
public class Role implements Serializable{

	private static final long serialVersionUID = -5242194403379238924L;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	@Column( name = "NAME", length = 50 )
	@NotNull
	private String name;

	@ManyToMany( fetch = FetchType.EAGER )
	@JoinTable( name = "ROLE_AUTHORITY", joinColumns = { @JoinColumn( name = "FK_ID_ROLE", referencedColumnName = "ID" ) }, inverseJoinColumns = { @JoinColumn( name = "FK_ID_AUTHORITY", referencedColumnName = "ID" ) } )
	private List<Authority> authorities;

	@ManyToMany( mappedBy = "roles", fetch = FetchType.LAZY )
	private List<User> users;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public List<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities( List<Authority> authorities ) {
		this.authorities = authorities;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers( List<User> users ) {
		this.users = users;
	}

}
