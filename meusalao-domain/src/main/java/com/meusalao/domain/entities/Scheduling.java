package com.meusalao.domain.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.meusalao.domain.enums.Status;

@Entity
@Table( name = "SCHEDULING" )
public class Scheduling implements Serializable{
	
	private static final long serialVersionUID = -6295785039342253247L;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;
	
	@OneToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "FK_ID_CLIENT" )
	@JsonManagedReference
	private Client client;

	@OneToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "FK_ID_SERVICE" )
	private Service service;

	@OneToOne( fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.REFRESH } )
	@JoinColumn( name = "FK_ID_PROFESSIONAL" )
	@JsonManagedReference
	private Professional professional;

	// @ManyToMany( fetch = FetchType.LAZY, cascade = { CascadeType.MERGE,
	// CascadeType.REFRESH } )
	// @JoinTable( name = "scheduled_service_professional", joinColumns =
	// @JoinColumn( name = "fk_id_scheduled_service" ), inverseJoinColumns =
	// @JoinColumn( name = "fk_id_professional" ) )
	// private List<Professional> professionals;

	@Column( name = "DATE_SCHEDULED" )
	@Temporal( TemporalType.TIMESTAMP )
	private Date dateScheduled;

	@Column( name = "DATE_END_PREVISION" )
	@Temporal( TemporalType.TIMESTAMP )
	private Date dateEndPrevision;

	@Column( name = "START_TIME_VALUE" )
	private Integer startTimeValue;
	
	@Column( name = "STATUS" )
	@Enumerated( EnumType.STRING )
	private Status status;

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "FK_ID_PARENT_SCHEDULING" )
	@JsonBackReference
	private ParentScheduling parentScheduling;

	@Column( name = "IS_UNAVAILABLE" )
	private Boolean isUnavailable;

	@Column( name = "START_TIME_UNAVAILABLE_VALUE" )
	private Integer startTimeUnavailable;

	@Column( name = "END_TIME_UNAVAILABLE_VALUE" )
	private Integer endTimeUnavailable;

	@Column( name = "REASON_UNAVAILABLE" )
	private String reasonUnavailable;

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient( Client client ) {
		this.client = client;
	}

	public Service getService() {
		return service;
	}

	public void setService( Service service ) {
		this.service = service;
	}

	public Professional getProfessional() {
		return professional;
	}

	public void setProfessional( Professional professional ) {
		this.professional = professional;
	}

	public Date getDateScheduled() {
		return dateScheduled;
	}

	public void setDateScheduled( Date dateScheduled ) {
		this.dateScheduled = dateScheduled;
	}

	public Date getDateEndPrevision() {
		return dateEndPrevision;
	}

	public void setDateEndPrevision( Date dateEndPrevision ) {
		this.dateEndPrevision = dateEndPrevision;
	}

	public Integer getStartTimeValue() {
		return startTimeValue;
	}

	public void setStartTimeValue( Integer startTimeValue ) {
		this.startTimeValue = startTimeValue;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus( Status status ) {
		this.status = status;
	}

	public ParentScheduling getParentScheduling() {
		return parentScheduling;
	}

	public void setParentScheduling( ParentScheduling parentScheduling ) {
		this.parentScheduling = parentScheduling;
	}

	public Boolean getIsUnavailable() {
		return isUnavailable;
	}

	public void setIsUnavailable( Boolean isUnavailable ) {
		this.isUnavailable = isUnavailable;
	}

	public Integer getStartTimeUnavailable() {
		return startTimeUnavailable;
	}

	public void setStartTimeUnavailable( Integer startTimeUnavailable ) {
		this.startTimeUnavailable = startTimeUnavailable;
	}

	public Integer getEndTimeUnavailable() {
		return endTimeUnavailable;
	}

	public void setEndTimeUnavailable( Integer endTimeUnavailable ) {
		this.endTimeUnavailable = endTimeUnavailable;
	}

	public String getReasonUnavailable() {
		return reasonUnavailable;
	}

	public void setReasonUnavailable( String reasonUnavailable ) {
		this.reasonUnavailable = reasonUnavailable;
	}

}
