package com.meusalao.domain.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.meusalao.domain.enums.TypeValue;

@Entity
@Table( name = "SERVICE" )
@JsonIgnoreProperties( { "hibernateLazyInitializer", "handler" } )
public class Service implements Serializable {

	private static final long serialVersionUID = -5913354335593496363L;

	@Id
	@Column( name = "ID" )
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;

	@Column( name = "NAME" )
	private String name;

	@Column( name = "COMMISSION" )
	private Integer commission;

	@Column( name = "TIME_PREDICTION" )
	private Integer timePrediction;

	@Column( name = "VISIBLE_TO_CLIENT" )
	private Boolean visibleToClient;

	@Column( name = "TYPE_VALUE" )
	@Enumerated( EnumType.STRING )
	private TypeValue typeValue;

	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "FK_ID_CATEGORY" )
	private Category category;

	@OneToMany( mappedBy = "service", cascade = CascadeType.REMOVE )
	private List<Price> prices;

	@ManyToMany
	@JoinTable( name = "professional_service", joinColumns = { @JoinColumn( name = "fk_id_service" ) }, inverseJoinColumns = { @JoinColumn( name = "fk_id_professional" ) } )
	@JsonManagedReference
	private List<Professional> professionals;

	public Category getCategory() {
		return category;
	}

	public void setCategory( Category category ) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission( Integer commission ) {
		this.commission = commission;
	}

	public Integer getTimePrediction() {
		return timePrediction;
	}

	public void setTimePrediction( Integer timePrediction ) {
		this.timePrediction = timePrediction;
	}

	public Boolean getVisibleToClient() {
		return visibleToClient;
	}

	public void setVisibleToClient( Boolean visibleToClient ) {
		this.visibleToClient = visibleToClient;
	}

	public List<Price> getPrices() {
		return prices;
	}

	public void setPrices( List<Price> prices ) {
		this.prices = prices;
	}

	public TypeValue getTypeValue() {
		return typeValue;
	}

	public void setTypeValue( TypeValue typeValue ) {
		this.typeValue = typeValue;
	}

	public List<Professional> getProfessionals() {
		if ( professionals == null ) {
			return new ArrayList<>();
		}
		return professionals;
	}

	public void setProfessionals( List<Professional> professionals ) {
		this.professionals = professionals;
	}

}
