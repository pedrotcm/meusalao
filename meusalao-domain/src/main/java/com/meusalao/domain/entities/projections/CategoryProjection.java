package com.meusalao.domain.entities.projections;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.meusalao.domain.entities.Category;
import com.meusalao.domain.entities.Service;

@Projection( name = "fullData", types = Category.class )
public interface CategoryProjection {

	Long getId();

	String getDescription();

	List<Service> getServices();
}
