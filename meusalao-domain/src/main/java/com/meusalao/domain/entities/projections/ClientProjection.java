package com.meusalao.domain.entities.projections;

import org.springframework.data.rest.core.config.Projection;

import com.meusalao.domain.entities.Address;
import com.meusalao.domain.entities.Client;
import com.meusalao.domain.enums.ChannelsMetSalon;

@Projection( name = "fullData", types = Client.class )
public interface ClientProjection {

	Long getId();

	String getName();

	String getBirthday();

	String getPhone();

	String getPhone2();

	String getEmail();

	Address getAddress();

	ChannelsMetSalon getChannelMetSalon();

}
