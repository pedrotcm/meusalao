package com.meusalao.domain.entities.projections;

import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.enums.Frequency;
import com.meusalao.domain.enums.Period;

@Projection( name = "fullData", types = ParentScheduling.class )
public interface ParentSchedulingProjection {

	Long getId();

	Date getCreationDate();

	Frequency getFrequency();

	Period getPeriod();

	List<Scheduling> getSchedulings();

}
