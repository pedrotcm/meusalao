package com.meusalao.domain.entities.projections;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.meusalao.domain.entities.Address;
import com.meusalao.domain.entities.Professional;
import com.meusalao.domain.entities.Service;
import com.meusalao.domain.enums.ContractType;

@Projection( name = "fullData", types = Professional.class )
public interface ProfessionalProjection {

	Long getId();

	String getName();

	String getNickname();

	ContractType getContractType();

	Boolean getJustArrivalOrder();

	String getBirthday();

	String getPhone();

	String getPhone2();

	String getEmail();

	Address getAddress();

	String getRg();

	String getCpf();

	List<Service> getServices();
}
