package com.meusalao.domain.entities.projections;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.meusalao.domain.entities.Client;
import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.Professional;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.entities.Service;
import com.meusalao.domain.enums.Status;

@Projection( name = "fullData", types = Scheduling.class )
public interface SchedulingProjection {

	Long getId();

	Client getClient();

	Service getService();

	Professional getProfessional();

	@JsonFormat( timezone = "Brazil/East" )
	Date getDateScheduled();

	// @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy
	// HH:mm", timezone = "Brazil/East" )
	@JsonFormat( timezone = "Brazil/East" )
	Date getDateEndPrevision();

	Integer getStartTimeValue();

	Status getStatus();

	ParentScheduling getParentScheduling();

	Boolean getIsUnavailable();

	Integer getStartTimeUnavailable();

	Integer getEndTimeUnavailable();

	String getReasonUnavailable();

}
