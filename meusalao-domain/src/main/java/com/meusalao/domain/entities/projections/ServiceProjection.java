package com.meusalao.domain.entities.projections;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.meusalao.domain.entities.Category;
import com.meusalao.domain.entities.Price;
import com.meusalao.domain.entities.Professional;
import com.meusalao.domain.entities.Service;
import com.meusalao.domain.enums.TypeValue;

@Projection( name = "fullData", types = Service.class )
public interface ServiceProjection {

	Long getId();

	String getName();

	Integer getCommission();

	Integer getTimePrediction();

	Boolean getVisibleToClient();

	TypeValue getTypeValue();

	Category getCategory();

	List<Price> getPrices();

	List<Professional> getProfessionals();

}
