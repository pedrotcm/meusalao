package com.meusalao.domain.enums;

public enum AuthorityCode {
	MANAGE_CLIENTS,
	MANAGE_PROFESSIONALS,
	MANAGE_SERVICES,
	MANAGE_SCHEDULES
}