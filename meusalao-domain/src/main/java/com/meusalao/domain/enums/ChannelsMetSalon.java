package com.meusalao.domain.enums;

public enum ChannelsMetSalon {

	TV,
	EMAIL,
	FACEBOOK,
	GOOGLE,
	FRIEND_INDICATE,
	INSTAGRAM,
	RADIO,
	OTHER

}
