package com.meusalao.domain.enums;

public enum ContractType {

	COMISSION,
	FIXED_VALUE;

}
