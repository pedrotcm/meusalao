package com.meusalao.domain.enums;

public enum Frequency {

	JUST_SCHEDULED,
	WEEKLY,
	FORTNIGHTLY,
	MONTHLY

}
