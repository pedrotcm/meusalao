package com.meusalao.domain.enums;

public enum Status {

	/* Scheduling */
	SCHEDULED,
	LATE,
	ORDER_STARTED,

	/* Order */
	OPENED,
	PENDENT,
	FINISHED,

	/* Scheduling | Order */
	CANCELED,

	/* Unavaiable */
	UNAVAILABLE
}
