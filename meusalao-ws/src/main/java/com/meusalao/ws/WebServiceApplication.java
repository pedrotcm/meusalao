package com.meusalao.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan( "com.meusalao.domain.entities" )
public class WebServiceApplication {

	public static void main( String[] args ) {
		SpringApplication.run( WebServiceApplication.class, args );
	}

}
