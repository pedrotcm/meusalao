package com.meusalao.ws.configs;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import com.meusalao.domain.entities.Address;
import com.meusalao.domain.entities.Category;
import com.meusalao.domain.entities.Client;
import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.Price;
import com.meusalao.domain.entities.Professional;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.entities.Service;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		super.configureRepositoryRestConfiguration( config );
		config.exposeIdsFor( Client.class, Category.class, Professional.class, Address.class, Service.class, Price.class, ParentScheduling.class, Scheduling.class );
    }
}