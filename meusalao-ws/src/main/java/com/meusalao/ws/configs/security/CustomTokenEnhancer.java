package com.meusalao.ws.configs.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.mobile.device.Device;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

public class CustomTokenEnhancer implements TokenEnhancer {

	static final String CLAIM_KEY_USERNAME = "sub";
	static final String CLAIM_KEY_AUDIT = "audit";
	static final String CLAIM_KEY_CREATED = "created";
	static final String CLAIM_KEY_ROLE = "role";

	private static final String AUDIT_UNKNOWN = "unknown";
	private static final String AUDIT_WEB = "web";
	private static final String AUDIT_MOBILE = "mobile";
	private static final String AUDIT_TABLET = "tablet";

	@Override
	public OAuth2AccessToken enhance( OAuth2AccessToken accessToken, OAuth2Authentication authentication ) {
		Map<String, Object> additionalInfo = new HashMap<>();

		additionalInfo.put( CLAIM_KEY_USERNAME, authentication.getOAuth2Request().getRequestParameters().get( "username" ) );
		// additionalInfo.put(CLAIM_KEY_AUDIT, generateAudience(device));
		additionalInfo.put( CLAIM_KEY_CREATED, new Date() );
		additionalInfo.put( CLAIM_KEY_ROLE, authentication.getAuthorities() );

		( (DefaultOAuth2AccessToken) accessToken ).setAdditionalInformation( additionalInfo );
		return accessToken;
	}

	private String generateAudit( Device device ) {
		String audit = AUDIT_UNKNOWN;
		if ( device.isNormal() ) {
			audit = AUDIT_WEB;
		} else if ( device.isTablet() ) {
			audit = AUDIT_TABLET;
		} else if ( device.isMobile() ) {
			audit = AUDIT_MOBILE;
		}
		return audit;
	}

}
