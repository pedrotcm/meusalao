package com.meusalao.ws.configs.security;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.meusalao.domain.entities.Role;
import com.meusalao.domain.entities.User;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getUsername(),
                user.getFirstname(),
                user.getLastname(),
                user.getEmail(),
                user.getPassword(),
				mapToGrantedAuthorities( user.getRoles() ),
                user.getEnabled(),
                user.getLastPasswordResetDate()
        );
    }

	private static Set<SimpleGrantedAuthority> mapToGrantedAuthorities( List<Role> roles ) {
		Set<SimpleGrantedAuthority> totalAuthorities = new HashSet<>();
		roles.forEach( r -> {
			List<SimpleGrantedAuthority> authorities = r.getAuthorities().stream().map( authority -> new SimpleGrantedAuthority( authority.getCode().name() ) ).collect( Collectors.toList() );
			totalAuthorities.addAll( authorities );
		} );

		return totalAuthorities;
    }
}
