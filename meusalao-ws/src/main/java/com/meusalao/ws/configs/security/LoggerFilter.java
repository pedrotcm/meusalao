package com.meusalao.ws.configs.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.GenericFilterBean;

public class LoggerFilter extends GenericFilterBean {

	private static final Logger logger = LoggerFactory.getLogger( LoggerFilter.class );

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {

		if ( request instanceof HttpServletRequest ) {
			logger.info( String.format( "%s - %s", request.getRemoteHost(), ( (HttpServletRequest) request ).getRequestURL().toString() ) );
		}
		chain.doFilter( request, response );
	}
}