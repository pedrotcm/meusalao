package com.meusalao.ws.configs.security.configs;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.meusalao.ws.configs.security.CustomTokenEnhancer;

@Configuration
@EnableAuthorizationServer
public class Oauth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Value( "${oauth2.resource.id}" )
	private String resourceId;

	@Value( "${oauth2.secret}" )
	private String secret;

	@Value( "${oauth2.access_token.validity}" )
	private Integer accessTokenValidity;

	@Value( "${oauth2.refresh_token.validity}" )
	private Integer refreshTokenValidity;

	@Override
	public void configure( AuthorizationServerEndpointsConfigurer endpoints ) throws Exception {
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers( Arrays.asList( tokenEnhancer(), accessTokenConverter() ) );

		endpoints.tokenStore( tokenStore() ).tokenEnhancer( tokenEnhancerChain ).authenticationManager( authenticationManager );
	}

	@Override
	public void configure( AuthorizationServerSecurityConfigurer oauthServer ) throws Exception {
		oauthServer.tokenKeyAccess( "permitAll()" ).checkTokenAccess( "isAuthenticated()" );
	}

	@Override
	public void configure( ClientDetailsServiceConfigurer clients ) throws Exception {
		clients.inMemory()
			.withClient( "meusalao-server")
			.authorizedGrantTypes( "password","refresh_token" )
				.resourceIds( resourceId )
				.scopes( "full" )
				.secret( secret )
				.accessTokenValiditySeconds( accessTokenValidity )
				.refreshTokenValiditySeconds( refreshTokenValidity );
	}

	@Bean
	public TokenEnhancer tokenEnhancer() {
		return new CustomTokenEnhancer();
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore( accessTokenConverter() );
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey( secret );
		return converter;
	}


}
