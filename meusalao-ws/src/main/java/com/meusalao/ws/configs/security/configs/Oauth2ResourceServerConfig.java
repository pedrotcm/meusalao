package com.meusalao.ws.configs.security.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.mobile.device.DeviceResolverRequestFilter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableResourceServer
public class Oauth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Value( "${oauth2.resource.id}" )
	private String resourceId;

	@Override
	public void configure( ResourceServerSecurityConfigurer config ) {
		config.resourceId( resourceId );
	}

	// @Bean
	// public CorsFilter corsFilter() throws Exception {
	// return new CorsFilter();
	// }

	@Override
	public void configure( HttpSecurity httpSecurity ) throws Exception {
		httpSecurity
				// we don't need CSRF because our token is invulnerable
				.csrf().disable()

				// .exceptionHandling().authenticationEntryPoint(
				// unauthorizedHandler ).accessDeniedHandler(
				// accessDeniedHandler ).and()

				// don't create session
				.sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS ).and()

				.authorizeRequests().antMatchers( "/h2/**" ).permitAll().antMatchers( HttpMethod.OPTIONS, "/**" ).permitAll().anyRequest().authenticated();

		// Custom JWT based security filter
		httpSecurity.addFilterBefore( new DeviceResolverRequestFilter(), UsernamePasswordAuthenticationFilter.class );
		// httpSecurity.addFilterBefore( corsFilter(),
		// UsernamePasswordAuthenticationFilter.class );

		// disable page caching
		httpSecurity.headers().cacheControl();
		httpSecurity.headers().frameOptions().sameOrigin();

	}


}
