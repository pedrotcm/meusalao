package com.meusalao.ws.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.meusalao.domain.dtos.ClientDto;
import com.meusalao.domain.entities.Client;
import com.meusalao.ws.services.ClientService;

@RepositoryRestController
@RequestMapping( "/clients" )
public class ClientController {

	@Autowired
	private ClientService clientService;

	@PostMapping
	public ResponseEntity<?> addClient( @RequestBody ClientDto client ) {

		Client entity = clientService.save( client.toEntity() );

		return ResponseEntity.ok( true );
	}

	// @GetMapping
	// public ResponseEntity<?> getProfessionals() {
	// return ResponseEntity.ok( professionalService.findAll() );
	// }

}
