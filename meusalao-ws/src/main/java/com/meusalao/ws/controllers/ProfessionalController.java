package com.meusalao.ws.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.meusalao.domain.dtos.ProfessionalDto;
import com.meusalao.domain.entities.Professional;
import com.meusalao.ws.services.ProfessionalService;

@RepositoryRestController
@RequestMapping( "/professionals" )
public class ProfessionalController {

	@Autowired
	private ProfessionalService professionalService;

	@PostMapping
	public ResponseEntity<?> addProfessional( @RequestBody ProfessionalDto professional ) {

		Professional entity = professionalService.save( professional.toEntity() );

		return ResponseEntity.ok( true );
	}

	// @GetMapping
	// public ResponseEntity<?> getProfessionals() {
	// return ResponseEntity.ok( professionalService.findAll() );
	// }

}
