package com.meusalao.ws.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.meusalao.domain.dtos.ParentSchedulingDto;
import com.meusalao.domain.dtos.ParentUnavailableDto;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.enums.Status;
import com.meusalao.ws.services.ParentSchedulingService;

@RepositoryRestController
@RequestMapping( "/schedulings" )
public class SchedulingController {

	@Autowired
	private ParentSchedulingService parentSchedulingService;

	@PostMapping
	public ResponseEntity<?> addScheduling( @RequestBody ParentSchedulingDto parentScheduling ) {

		parentSchedulingService.save( parentScheduling.toEntity() );

		return ResponseEntity.ok( true );
	}

	@PostMapping( "/unavailable" )
	public ResponseEntity<?> addUnavailable( @RequestBody ParentUnavailableDto parentUnavailable ) {

		parentSchedulingService.save( parentUnavailable.toEntity() );

		return ResponseEntity.ok( true );
	}

	@PostMapping( "/cancel" )
	public ResponseEntity<?> cancelScheduling( @RequestBody ParentSchedulingDto parentScheduling ) {

		parentSchedulingService.cancel( parentScheduling );

		return ResponseEntity.ok( true );
	}

	@GetMapping( "/paginated" )
	public ResponseEntity<?> findPaginated( @RequestParam( "startDate" ) Date startDate, @RequestParam( "endDate" ) Date endDate, @RequestParam( "status" ) Status status, @RequestParam( "clientId" ) Integer clientId, @RequestParam( "professionalId" ) Integer professionalId, @RequestParam( "page" ) int page, @RequestParam( "size" ) int size, @RequestParam( "sortField" ) String sortField, @RequestParam( "sortOrder" ) Integer sortOrder ) {
		Page<Scheduling> pageScheduling = parentSchedulingService.findPaginated( startDate, endDate, status, clientId, professionalId, page, size, sortField, sortOrder );
		return ResponseEntity.ok( pageScheduling );
	}

}
