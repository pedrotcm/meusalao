package com.meusalao.ws.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.meusalao.domain.dtos.ServiceDto;
import com.meusalao.domain.entities.Service;
import com.meusalao.ws.services.ServiceService;

@RepositoryRestController
@RequestMapping( "/services" )
public class ServiceController {

	@Autowired
	private ServiceService serviceService;

	@PostMapping
	public ResponseEntity<?> addService( @RequestBody ServiceDto service ) {

		Service entity = serviceService.save( service.toEntity() );

		return ResponseEntity.ok( true );
	}

	// @GetMapping
	// public ResponseEntity<?> getServices() {
	// return ResponseEntity.ok( serviceService.findAll() );
	// }

}
