package com.meusalao.ws.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.meusalao.domain.entities.Category;
import com.meusalao.domain.entities.projections.CategoryProjection;

@RepositoryRestResource( collectionResourceRel = "categories", path = "categories", excerptProjection = CategoryProjection.class )
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
