package com.meusalao.ws.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.meusalao.domain.entities.Client;
import com.meusalao.domain.entities.projections.ClientProjection;

@RepositoryRestResource( collectionResourceRel = "clients", path = "clients", excerptProjection = ClientProjection.class )
public interface ClientRepository extends JpaRepository<Client, Long> {

	@Query( "from Client c where c.name like CONCAT('%', lower(:name), '%')" )
	List<Client> findByName( @Param( "name" ) String name );

}
