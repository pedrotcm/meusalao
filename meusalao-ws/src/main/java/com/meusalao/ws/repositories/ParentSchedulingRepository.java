package com.meusalao.ws.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.projections.ParentSchedulingProjection;

@RepositoryRestResource( collectionResourceRel = "parentschedulings", path = "parentschedulings", excerptProjection = ParentSchedulingProjection.class )
public interface ParentSchedulingRepository extends JpaRepository<ParentScheduling, Long> {

}
