package com.meusalao.ws.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.meusalao.domain.entities.Professional;
import com.meusalao.domain.entities.projections.ProfessionalProjection;

@RepositoryRestResource( collectionResourceRel = "professionals", path = "professionals", excerptProjection = ProfessionalProjection.class )
public interface ProfessionalRepository extends JpaRepository<Professional, Long> {

	@Query( "from Professional p where lower(p.nickname) like CONCAT('%', lower(:nickname), '%')" )
	Professional findByNickname( @Param( "nickname" ) String nickname );

	List<Professional> findByServicesId( @Param( "id" ) Long id );

}
