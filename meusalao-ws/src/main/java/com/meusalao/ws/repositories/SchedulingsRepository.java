package com.meusalao.ws.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.entities.projections.SchedulingProjection;
import com.meusalao.domain.enums.Status;

@RepositoryRestResource( collectionResourceRel = "schedulings", path = "schedulings", excerptProjection = SchedulingProjection.class )
public interface SchedulingsRepository extends JpaRepository<Scheduling, Long> {

	List<Scheduling> findByStatusInAndProfessionalIdAndDateScheduledBetween( @Param( "status" ) List<Status> status, @Param( "idProfessional" ) Long idProfessional, @Param( "dateStart" ) Date dateStart, @Param( "dateEnd" ) Date dateEnd );

	@Query( "FROM Scheduling s WHERE "
			+ "s.dateScheduled BETWEEN ?1 AND ?2 "
			+ "AND s.status = ?3 "
			+ "AND ( CASE WHEN ?4 != -1 THEN s.client.id ELSE -1 END ) = ?4 "
			+ "AND ( CASE WHEN ?5 != -1 THEN s.professional.id ELSE -1 END ) = ?5" )
	Page<Scheduling> findPaginated( Date startDate, Date endDate, Status status, Integer clientId, Integer professionalId, Pageable pageable );
}
