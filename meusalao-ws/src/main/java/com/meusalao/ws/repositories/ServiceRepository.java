package com.meusalao.ws.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.meusalao.domain.entities.Service;
import com.meusalao.domain.entities.projections.ServiceProjection;

@RepositoryRestResource( collectionResourceRel = "services", path = "services", excerptProjection = ServiceProjection.class )
public interface ServiceRepository extends JpaRepository<Service, Long> {

	List<Service> findByProfessionalsId( @Param( "idProfessional" ) Long id );

}
