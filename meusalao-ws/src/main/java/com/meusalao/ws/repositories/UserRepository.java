package com.meusalao.ws.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.meusalao.domain.entities.User;

@RepositoryRestResource( exported = false )
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

}
