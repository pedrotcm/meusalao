package com.meusalao.ws.services;

import java.util.Date;

import org.springframework.data.domain.Page;

import com.meusalao.domain.dtos.ParentSchedulingDto;
import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.enums.Status;

public interface ParentSchedulingService extends BaseService<ParentScheduling> {

	Scheduling cancel( ParentSchedulingDto parentScheduling );

	Page<Scheduling> findPaginated( Date startDate, Date endDate, Status status, Integer clientId, Integer professionalId, int page, int size, String sortField, Integer sortOrder );
}
