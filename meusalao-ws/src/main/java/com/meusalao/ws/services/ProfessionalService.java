package com.meusalao.ws.services;

import com.meusalao.domain.entities.Professional;

public interface ProfessionalService extends BaseService<Professional> {

	Professional findByNickname( String nickname );

}
