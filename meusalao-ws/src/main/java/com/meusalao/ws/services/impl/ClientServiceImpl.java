package com.meusalao.ws.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meusalao.domain.entities.Client;
import com.meusalao.ws.repositories.ClientRepository;
import com.meusalao.ws.services.ClientService;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository clientRepository;

	@Override
	public Client save( Client entity ) {
		return clientRepository.save( entity );
	}

	@Override
	public Client load( Long id ) {
		return clientRepository.findOne( id );
	}

	@Override
	public void delete( Client entity ) {
		clientRepository.delete( entity );
	}

	@Override
	public List<Client> findAll() {
		return clientRepository.findAll();
	}

}
