package com.meusalao.ws.services.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.meusalao.domain.dtos.ParentSchedulingDto;
import com.meusalao.domain.entities.ParentScheduling;
import com.meusalao.domain.entities.Scheduling;
import com.meusalao.domain.enums.Status;
import com.meusalao.ws.repositories.ParentSchedulingRepository;
import com.meusalao.ws.repositories.SchedulingsRepository;
import com.meusalao.ws.services.ParentSchedulingService;

@Service
@Transactional
public class ParentSchedulingServiceImpl implements ParentSchedulingService {

	@Autowired
	private ParentSchedulingRepository parentSchedulingRepository;

	@Autowired
	private SchedulingsRepository schedulingRepository;

	@Override
	public ParentScheduling save( ParentScheduling entity ) {

		return parentSchedulingRepository.save( entity );
	}

	@Override
	public ParentScheduling load( Long id ) {
		return parentSchedulingRepository.findOne( id );
	}

	@Override
	public void delete( ParentScheduling entity ) {
		parentSchedulingRepository.delete( entity );
	}

	@Override
	public List<ParentScheduling> findAll() {
		return parentSchedulingRepository.findAll();
	}

	@Override
	public Scheduling cancel( ParentSchedulingDto parentScheduling ) {
		Scheduling scheduling = schedulingRepository.findOne( parentScheduling.getIdScheduling() );
		scheduling.setStatus( Status.CANCELED );
		return schedulingRepository.save( scheduling );
	}

	@Override
	public Page<Scheduling> findPaginated( Date startDate, Date endDate, Status status, Integer clientId, Integer professionalId, int page, int size, String sortField, Integer sortOrder ) {
		Sort sort = new Sort( sortOrder == 1 ? Sort.Direction.ASC : Sort.Direction.DESC, "dateScheduled" );
		if ( !StringUtils.isEmpty( sortField ) && !sortField.equals( "undefined" ) ) {
			 sort = new Sort( sortOrder == 1 ? Sort.Direction.ASC : Sort.Direction.DESC, sortField );
		}
		
		return schedulingRepository.findPaginated( startDate, endDate, status, clientId, professionalId, new PageRequest( page, size, sort ) );
	}
}
