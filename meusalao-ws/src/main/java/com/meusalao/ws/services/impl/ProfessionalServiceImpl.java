package com.meusalao.ws.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.meusalao.domain.entities.Professional;
import com.meusalao.ws.repositories.ProfessionalRepository;
import com.meusalao.ws.services.ProfessionalService;

@Service
@Transactional
public class ProfessionalServiceImpl implements ProfessionalService {

	@Autowired
	private ProfessionalRepository professionalRepository;

	@Override
	public Professional save( Professional entity ) {
		return professionalRepository.save( entity );
	}

	@Override
	public Professional load( Long id ) {
		return professionalRepository.findOne( id );
	}

	@Override
	public void delete( Professional entity ) {
		professionalRepository.delete( entity );
	}

	@Override
	public List<Professional> findAll() {
		return professionalRepository.findAll();
	}

	@Override
	public Professional findByNickname( String nickname ) {
		return professionalRepository.findByNickname( nickname );
	}

}
