package com.meusalao.ws.services.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meusalao.ws.repositories.ServiceRepository;
import com.meusalao.ws.services.ServiceService;

@Service
@Transactional
public class ServiceServiceImpl implements ServiceService {

	@Autowired
	private ServiceRepository serviceRepository;

	@Override
	public com.meusalao.domain.entities.Service save( com.meusalao.domain.entities.Service entity ) {
		return serviceRepository.save( entity );
	}

	@Override
	public com.meusalao.domain.entities.Service load( Long id ) {
		return serviceRepository.findOne( id );
	}

	@Override
	public void delete( com.meusalao.domain.entities.Service entity ) {
		serviceRepository.delete( entity );
	}

	@Override
	public List<com.meusalao.domain.entities.Service> findAll() {
		return serviceRepository.findAll();
	}

}
